const meowSayers = {}

chrome.tabs.onUpdated.addListener(function(tabId, props, tab) {
  if (tab.audible) {
    meowSayers[tabId] = {
      tab,
      time: Math.floor((new Date()).getTime() / 1000)
    };

  } else {
    if (typeof meowSayers[tabId] !== 'undefined') {
      // delete meowSayers[tabId];
    }
  }
});

chrome.tabs.onRemoved.addListener(function(tabId) {
  if (typeof meowSayers[tabId] !== 'undefined') {
    delete meowSayers[tabId];
  }
});

chrome.runtime.onMessage.addListener(
function(request, sender, sendResponse) {
  if (request.meowtype === 'meowsayers') {
    sendResponse({meowSayers});
  }
});