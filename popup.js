const many = (num, noun) => {
  return num + ' ' + (num > 1 ? noun + 's' : noun) + ' ago';
}

const timeDelta = (time) => {
  if (time > 86400) {
    return 'long ago';

  } else if (time > 3600) {
    return many(Math.round(time / 3600), 'hour');

  } else if (time > 60) {
    return many(Math.round(time / 60), 'minute');

  } else if (time > 0) {
    return many(time, 'second');

  } else {
    return 'now';
  }
}

const switchToTab = tabId => {
  chrome.tabs.query({active: true}, tabs => {
    tabs.forEach(tab => chrome.tabs.update(tab.id, {highlighted: false}));
    chrome.tabs.update(tabId, {highlighted: true});
  });
}

chrome.runtime.sendMessage({meowtype: "meowsayers"}, function(response) {
  const list = document.querySelector('.list');
  list.innerHTML = '';

  if (!response || typeof response.meowSayers == 'undefined') return;

  const meowList = Object.keys(response.meowSayers).map(key => {
    const {time, tab} = response.meowSayers[key];
    return {
      id: tab.id,
      window: tab.windowId,
      title: tab.title,
      time,
      icon: tab.favIconUrl ? tab.favIconUrl : false,
    }
  });

  if (meowList.length) {
    meowList.sort((a, b) => b.time - a.time);
    const now = Math.floor((new Date()).getTime() / 1000);

    meowList.forEach(item => {
      const listItem = document.createElement('div');
      listItem.classList.add('item');

      if (item.icon) {
        const icon = document.createElement('img');
        icon.classList.add('icon');
        icon.innerHTML = '12 лет назад';
        icon.src = item.icon;
        listItem.appendChild(icon);
      }

      const text = document.createElement('span');
      text.classList.add('title');
      text.innerHTML = item.title;
      listItem.appendChild(text);

      const time = document.createElement('span');
      time.classList.add('time');
      time.innerHTML = timeDelta(now - item.time);
      listItem.appendChild(time);

      list.appendChild(listItem);

      listItem.addEventListener('click', () => {
        chrome.windows.getCurrent({}, win => {
          if (win.id !== item.window) {
            chrome.windows.update(item.window, {focused: true}, () => switchToTab(item.id));
          } else {
            switchToTab(item.id);
          }
        });
      })
    })

  } else {
    list.innerHTML = '<div class="nothing">No tabs were audioactive recently.</div>';
  }
});
